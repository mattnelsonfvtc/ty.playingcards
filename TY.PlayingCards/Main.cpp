// PlayingCards Lab Excercise
// Tim Yang

#include <iostream>
#include <conio.h>
#include <stdio.h>

using namespace std;

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Clubs, Diamonds, Hearts, Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};

void printCard(Card card);
void printCard(Card card);

int main()
{
	Card card;
	printCard(card);

	_getch();
	return 0;
}

void printCard(Card card)
{
	std::cout << "The ";
	switch (card.rank)
	{
	case Two: std::cout << "two"; break;
	case Three: std::cout << "three"; break;
	case Four: std::cout << "four"; break;
	case Five: std::cout << "five"; break;
	case Six: std::cout << "six"; break;
	case Seven: std::cout << "seven"; break;
	case Eight: std::cout << "eight"; break;
	case Nine: std::cout << "nine"; break;
	case Ten: std::cout << "ten"; break;
	case Jack: std::cout << "jack"; break;
	case Queen: std::cout << "queen"; break;
	case King: std::cout << "king"; break;
	case Ace: std::cout << "ace"; break;
	}

		std::cout << " of";

	switch (card.suit)
	{
	case Clubs: std::cout << "Clubs \n"; break;
	case Diamonds: std::cout << "Diamonds \n"; break;
	case Hearts: std::cout << "Hearts \n"; break;
	case Spades: std::cout << "Spades \n"; break;
	}

}

Card highCard(Card card1, Card card2)
{
	if (card1.rank >= card2.rank) return card1;
	return card2;
}